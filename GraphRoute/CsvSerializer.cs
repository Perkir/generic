using System;
using System.Linq;
using System.Collections.Generic;
using Graph;

namespace GraphRoute
{
    public static class CsvSerialize
	{
		public static List<string> ToCsvFormEdge<T>(this Graph<INode<T>,T> graph)
		{
		    return graph.EdgesList.Select(x => x.ToString()).ToList();
	    }
		public static List<Edge<INode<T>,T>> FromCsvToEdge<T>(this Graph<INode<T>, T> graph, string[] inputRows)
		{
		    var resultList = new List<Edge<INode<T>,T>>();
		    foreach (var row in inputRows)
		    {
			    var parts = row.Split('\t');
			    if (parts.Count() != 3) continue;
			    var beginNode = graph.VertexList.FirstOrDefault(w => w.Data.Equals(parts[0]));
			    var endNode = graph.VertexList.FirstOrDefault(w => w.Data.Equals(parts[1]));
			    int.TryParse(parts[2], out var weight);
			    if(weight.Equals(0))
			    {
				    throw new Exception($"Ошибка сериализации. Расстояние между городами {beginNode.Data} и {endNode.Data} должно быть больше 0") ;
			    }
			    resultList.Add( new Edge<INode<T>, T>((INode<T>) beginNode, (INode<T>) endNode, weight));
		    }            

		    return resultList;
	    }
    }
}
