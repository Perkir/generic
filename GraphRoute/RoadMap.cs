using System.Collections.Generic;
using System.Linq;
using System.Text;
using Graph;

namespace GraphRoute
{
    public class RoadMap<TU,T> : Graph<TU,T> where TU: INode<T>
    {
        public string FindRoute(T begin, T end)
        {
            if (!Contains(begin))
            {
                return $"Город {begin} отсутствует в списке доступных";
            }

            if (!Contains(end))
            {
                return $"Город {end} отсутствует в списке доступных";
            }

            var idBegin = GetIdByData(begin);
            var idEnd = GetIdByData(end);
            var vertexListCount = VertexList.Count;
            var curId = idBegin;
            var nextId = idBegin;
           
            var vizitVertex = new int[vertexListCount];

            for (var i = 0; i < vertexListCount; i++)
            {
                vizitVertex[i] = 0;
            }                

            UpdateWeight(idBegin, 0);

            CountWeight(vertexListCount, vizitVertex, curId, nextId );

            var routeList = RouteList(idEnd, idBegin);

            var routeString = new StringBuilder();
            var distance = 0;
            var routeArray = routeList.ToArray();
            for (var i = routeArray.Length - 1; i >= 0; i--)
            {
                if (i == 0) continue;
                var queryBeeline = EdgesList.FirstOrDefault(w => w.Begin.Id == routeArray[i] && w.End.Id == routeArray[i - 1]);
                if (queryBeeline == null) continue;
                distance += queryBeeline.Weight;
                routeString.Append($"{queryBeeline.GetRoute()}\n");
            }
            routeString.Append($"Общее расстояние: {distance} км");

            return routeString.ToString();
        }

        private List<int> RouteList(int idEnd, int idBegin)
        {
            var routeList = new List<int>() {idEnd};
            while (idEnd != idBegin)
            {
                var end = idEnd;
                var queryRoute = EdgesList.Where(w => w.Begin.Id == end);
                foreach (var item in queryRoute)
                {
                    var weight = item.Begin.Weight - item.Weight;
                    if (weight != item.End.Weight) continue;
                    idEnd = item.End.Id;
                    routeList.Add(idEnd);
                    break;
                }
            }

            return routeList;
        }

        private void CountWeight(int vertexListCount, IList<int> vizitVertex, int curId, int nextId)
        {
            for (var i = 0; i < vertexListCount; i++)
            {
                var curMinWeight = int.MaxValue;
                vizitVertex[curId - 1] = 1;
                var id = curId;
                var queryEdges = EdgesList.Where(w => w.Begin.Id == id);
                foreach (var item in queryEdges)
                {
                    if (vizitVertex[item.End.Id - 1] != 0) continue;
                    var weight = item.Begin.Weight + item.Weight;
                    if (weight < item.End.Weight)
                    {
                        UpdateWeight(item.End.Id, weight);
                    }

                    if (item.Weight >= curMinWeight) continue;
                    curMinWeight = item.Weight;
                    nextId = item.End.Id;
                }

                curId = nextId;
            }
        }
    }
}
