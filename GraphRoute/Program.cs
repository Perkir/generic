﻿using System;
using System.Collections.Generic;
using System.IO;
using Graph;

namespace GraphRoute
{
    class Program
    {
        static void Main()
        {
            var roadMap = City.GetCities();
            try
            {
                const string pathFile = "City.csv";
                roadMap.EdgesList = roadMap.FromCsvToEdge(File.ReadAllLines(pathFile));

                Console.Write("\nВведите город начала маршрута: ");
                var beginCity = Console.ReadLine();
                Console.Write("Введите город конца маршрута: ");
                var endCity = Console.ReadLine();
                Console.WriteLine(roadMap.FindRoute(beginCity, endCity));
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine();
        }
    }
}
