using System;
using System.Collections.Generic;
using Graph;

namespace GraphRoute
{
    public static class City
    {

        public static RoadMap<INode<string>, string> GetCities() {
            var listCity = new List<Vertex<string>>()
            {
                new Vertex<string>(1, "Королев"),
                new Vertex<string>(2, "Москва"),
                new Vertex<string>(3, "Люберцы"),
                new Vertex<string>(4, "Балашиха"),
                new Vertex<string>(5, "Коломна"),
                new Vertex<string>(6, "Иваново")

            };
            var roadMap = new RoadMap<INode<string>, string>();
            Console.WriteLine("Список городов:");
           
            foreach (var city in listCity)
            {
                roadMap.Add(city);
                Console.WriteLine(city.Data);
            }

            return roadMap;
        }
    }
}
