namespace Graph
{
    public class Vertex<T> : INode<T>
    {
        public int Id { get; set; }
        public T Data { get; set; }
        public int Weight {get; set;}

        public Vertex(int id, T data)
        {
            Id = id;
            Data = data;
            Weight = 100;
        }

    }
}