namespace Graph
{
    public class Edge<V,T> where V : INode<T>
    {
        public V Begin { get; private set; }
        public V End { get; }
        public int Weight { get; }

        public Edge( V begin, V end, int weight)
        {
            Begin = begin;
            End = end;
            Weight = weight;
        }

        public override string ToString()
        {
            return $"{Begin.Data}\t{End.Data}\t{Weight}";
        }
        public string GetRoute()
        {
            return $"{Begin.Data}->{End.Data}: {Weight} км";
        }
    }
}