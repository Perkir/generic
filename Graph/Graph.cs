using System;
using System.Collections.Generic;
using System.Linq;

namespace Graph
{
    public class Graph<TU, T> where TU : INode<T>
    {
        public readonly List<TU> VertexList;
        public List<Edge<TU, T>> EdgesList;

        protected Graph()
        {
            VertexList = new List<TU>();
            EdgesList = new List<Edge<TU, T>>();
        }

        public void Add(TU vertex)
        {
            VertexList.Add(vertex);
        }

        protected bool Contains(T data)
        {
            var upperData = FirstLetterToUpper(data.ToString());
            var vertex = VertexList.FirstOrDefault(f => f.Data.Equals(upperData));
            return vertex != null;
        }

        public void Remove(int id)
        {
            var vertex = VertexList.FirstOrDefault(f => f.Id.Equals(id));
            if (vertex != null)
            {
                VertexList.Remove(vertex);
            }
        }

        public T GetDataById(int id)
        {
            var vertex = VertexList.FirstOrDefault(f => f.Id.Equals(id));
            return (T) (vertex != null ? vertex.Data : default);
        }

        public int? GetWeightById(int id)
        {
            var vertex = VertexList.FirstOrDefault(f => f.Id.Equals(id));
            if (vertex != null) return vertex.Weight;
            return null;
        }

        protected int GetIdByData(T data)
        {
            var upperData = FirstLetterToUpper(data.ToString());
            var vertex = VertexList.FirstOrDefault(f => f.Data.Equals(upperData));
            return vertex != null ? vertex.Id : default;
        }

        protected void UpdateWeight(int id, int weight)
        {
            var vertex = VertexList.FirstOrDefault(f => f.Id.Equals(id));
            if (vertex != null)
            {
                vertex.Weight = weight;
            }
        }
        
        private static string FirstLetterToUpper(string str)
        {
            if (str == null)
                return null;

            if (str.Length > 1)
                return char.ToUpper(str[0]) + str.Substring(1);

            return str.ToUpper();
        }
    }
}
