namespace Graph
{
    public interface INode<T>
    {
        int Id { get; set; }
        T Data { get; set; }
        int Weight { get; set; }
    }
}